package util;
import java.beans.FeatureDescriptor;
import java.util.Iterator;
import java.util.Stack;

import javax.el.ELContext;
import javax.el.ELResolver;
import javax.el.ValueExpression;
import javax.faces.component.UIComponent;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;

@SuppressWarnings("unchecked")
public class CustomComponentELResolver extends ELResolver {
	public CustomComponentELResolver() {}

	public Object getValue(ELContext elContext, Object base, Object property) {
		if(base == null && property == null) {
			return null;
		}
		
		if(base == null) {
			if(property.equals("component")) {
				UIComponent component = null;
				UIViewRoot view = FacesContext.getCurrentInstance().getViewRoot();
				Stack<UIComponent> stack = (Stack<UIComponent>) view.getAttributes().get("com.ibm.faces.COMPOSITE_STACK");
				if(stack != null && !stack.empty()) {
					component = (UIComponent) stack.peek();
				}
				if(component != null) {
					elContext.setPropertyResolved(true);
					return component;
				}
			}
		}
		else {
			if(base instanceof UIComponent) {
				elContext.setPropertyResolved(true);
				if(property.toString().equals("clientId")) {
					return ((UIComponent)base).getClientId(FacesContext.getCurrentInstance());
				}
				return ((UIComponent)base).getAttributes().get(property);
			}
		}
		
		return null;
	}

	public Class<?> getType(ELContext elContext, Object base, Object property) {
		if(base != null && base instanceof UIComponent) {
			elContext.setPropertyResolved(true);
			Object ret = ((UIComponent)base).getAttributes().get(property);
			if(ret != null) {
				return ret.getClass();
			}
		}
		
		return null;
	}

	public void setValue(ELContext elContext, Object base, Object property, Object value) {
		if(base != null && base instanceof UIComponent) {
			elContext.setPropertyResolved(true);
			ValueExpression ve = ((UIComponent)base).getValueExpression(property.toString());
			if(ve != null) {
				ve.setValue(elContext, value);
			}
			else {
				((UIComponent)base).getAttributes().put(property.toString(), value);
			}
		}
	}

	public Class<?> getCommonPropertyType(ELContext elContext, Object base) {
		return null;
	}

	public Iterator<FeatureDescriptor> getFeatureDescriptors(ELContext elContext, Object base) {
		return null;
	}

	public boolean isReadOnly(ELContext elContext, Object base, Object property) {
		return false;
	}
}
