package managedBeans;

public class CurrentUser {
	private static String username;
	private static String password;
	public static String getUsername() {
		return username;
	}
	public static void setUsername(String username) {
		CurrentUser.username = username;
	}
	public static String getPassword() {
		return password;
	}
	public static void setPassword(String password) {
		CurrentUser.password = password;
	}
}
