<!DOCTYPE HTML>
<%-- jsf:pagecode language="java" location="/src/pagecode/Login.java" --%><%-- /jsf:pagecode --%><%@page
	language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="h" uri="http://java.sun.com/jsf/html"%>
<%@taglib prefix="f" uri="http://java.sun.com/jsf/core"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/theme/Master.css"
	type="text/css">
<title>Login</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
</head>
<head>
<title>Login</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" title="Style"
	href="${pageContext.request.contextPath}/theme/stylesheet.css">
<link rel="stylesheet" type="text/css" title="Style"
	href="${pageContext.request.contextPath}/theme/tabpanel.css">
</head>
<f:view>
	<body
		background="${pageContext.request.contextPath}/theme/img/350723_efubhUn8oMttcqtp8b2OojOJD.jpg">
		<h:form id="form1" styleClass="form">
			<p align="center"></p>
			<p align="center">
				<h:outputText id="text1" styleClass="newsPageTitle"
					value="#{bundle.title_main1}" />
			</p>
			<br>
			<br>
			<br>
			<br>
			<br>
			<hr size="3" align="center" style="width: 1290px">
			<br>
			<table>
				<tbody>
					<tr>
						<td style="width: 291px" width="291"></td>
						<td style="width: 729px; height: 203px" height="203" width="686"><p
								align="center">
								<h:outputText id="lblUsername" styleClass="loginLabels"
									value="#{bundle.label_username}" />
								<h:inputText id="txtBoxUsername" styleClass="inputText"
									required="true" value="#{pc_Login.username}">
									<f:validateLength minimum="3" maximum="20" />
								</h:inputText>
							</p>
							<p align="center">
								<h:outputText id="lblPassword" styleClass="loginLabels"
									value="#{bundle.label_password}" />
								<h:inputSecret id="txtBoxPassword" styleClass="inputSecret"
									required="true" value="#{pc_Login.password}">
									<f:validateLength minimum="3" maximum="20" />
								</h:inputSecret>
							</p>
							<p align="center">
								<br> <br> <br> <br>
								<h:commandButton type="submit" value="#{bundle.button_login}"
									id="btnLogin" styleClass="btnRound"
									action="#{pc_Login.doBtnLoginAction}" />
							</p></td>
						<td style="width: 241px" height="203" width="337">
							<p align="center">
								<h:message for="txtBoxUsername" styleClass="error" />
								<br>
								<h:message for="txtBoxPassword" styleClass="error" />
							</p>
						</td>
					</tr>
				</tbody>
			</table>
			<hr size="3" align="center" style="width: 1290px">
			<p></p>
			<p align="left">
				<span class="newsCopyright">Copyright � <script
						type="text/javascript">
			var d = new Date();
			document.write(d.getFullYear());
			</script> Ahmed Anwar @ <a href="http://www.sumerge.com"> Sumerge Software
						Solutions</a> all rights reserved
				</span>
			</p>

			<p align="left"></p>

			<p align="left"></p>
		</h:form>
	</body>
</f:view>
</html>