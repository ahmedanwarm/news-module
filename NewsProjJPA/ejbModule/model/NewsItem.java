package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the NEWSITEM database table.
 * 
 *///
@Entity
@Table(name="NEWSITEM")
@NamedQueries({
	//@NamedQuery(name="getAllNewsIDs", query = "SELECT n.id FROM NewsItem n WHERE n.deleted='0'"),
	@NamedQuery(name="getAllNewsItems", query = "SELECT n FROM NewsItem n"),
	@NamedQuery(name="getNewsItemByID", query = "SELECT n FROM NewsItem n WHERE n.id = :selectedID")
	})
public class NewsItem implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID")
	private int id;

	@Column(name="BODY")
	private String body;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATECREATE")
	private Date datecreate;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATEMOD")
	private Date datemod;
	
	@Column(name="TITLE")
	private String title;
	
	@Column(name="DELETED")
	private char deleted;

	//bi-directional many-to-many association to Author
	@ManyToMany
	@JoinTable(name="AUTHNEWS",
	joinColumns=@JoinColumn(name="NEWSID"),
	inverseJoinColumns=@JoinColumn(name="AUTHID"))
	private List<Author> authors;

	public NewsItem() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getBody() {
		return this.body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public Date getDatecreate() {
		return this.datecreate;
	}

	public void setDatecreate(Date datecreate) {
		this.datecreate = datecreate;
	}

	public Date getDatemod() {
		return this.datemod;
	}

	public void setDatemod(Date datemod) {
		this.datemod = datemod;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<Author> getAuthors() {
		return this.authors;
	}

	public void setAuthors(List<Author> authors) {
		this.authors = authors;
	}

	public char getDeleted() {
		return deleted;
	}

	public void setDeleted(char deleted) {
		this.deleted = deleted;
	}

}