package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the AUTHOR database table.
 * 
 */
@Entity
@NamedQueries({
	@NamedQuery(name="getAllAuthors", query="SELECT a FROM Author a")
})
public class Author implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID")
	private Integer id;

	@Column(name="DOB")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dob;

	@Column(name="EMAIL")
	private String email;

	@Column(name="FNAME")
	private String fname;

	@Column(name="LNAME")
	private String lname;

	@Column(name="MNAME")
	private String mname;

	@Column(name="PHONENUMBER")
	private String phonenumber;
	
	@Column(name = "ADDRESSID")
	private int adressid;
	
	@Column(name="DELETED")
	private char deleted;
	
	//bi-directional many-to-many association to Author
	@ManyToMany
	@JoinTable(name="AUTHNEWS",
	joinColumns=@JoinColumn(name="AUTHID"),
	inverseJoinColumns=@JoinColumn(name="NEWSID"))
	private List<NewsItem> newsItems;

	public List<NewsItem> getNewsItems() {
		return newsItems;
	}

	public void setNewsItems(List<NewsItem> newsItems) {
		this.newsItems = newsItems;
	}

	@ManyToOne
	@JoinColumn(name="addressid")
	private Address address;

	public Address getAddress() {
		if(address == null)
			address = new Address();
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public Author() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getDob() {
		return this.dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFname() {
		return this.fname;
	}

	public void setFname(String fname) {
		this.fname = fname;
	}

	public String getLname() {
		return this.lname;
	}

	public void setLname(String lname) {
		this.lname = lname;
	}

	public String getMname() {
		return this.mname;
	}

	public void setMname(String mname) {
		this.mname = mname;
	}

	public String getPhonenumber() {
		return this.phonenumber;
	}

	public void setPhonenumber(String phonenumber) {
		this.phonenumber = phonenumber;
	}

	public int getAdressid() {
		return adressid;
	}

	public void setAdressid(int adressid) {
		this.adressid = adressid;
	}

	public char getDeleted() {
		return deleted;
	}

	public void setDeleted(char deleted) {
		this.deleted = deleted;
	}
}