package pagecode;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.*;

import managedBeans.*;
import model.*;
import beans.*;

import com.ibm.faces.component.html.HtmlScriptCollector;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.component.html.HtmlForm;
import com.ibm.faces.component.html.HtmlCommandExButton;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.event.ValueChangeEvent;
import com.ibm.faces.bf.component.html.HtmlBfPanel;
import com.ibm.faces.bf.component.html.HtmlTabbedPanel;
import javax.faces.component.html.HtmlSelectOneListbox;
import javax.faces.component.UISelectItems;
import javax.faces.component.html.HtmlPanelGroup;
import javax.faces.component.UISelectItem;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlInputTextarea;
import javax.faces.component.html.HtmlSelectManyListbox;
import com.ibm.faces.component.html.HtmlAjaxRefreshRequest;
import javax.faces.component.html.HtmlSelectOneMenu;

/**
 * @author aanwar
 *
 */
@ManagedBean
public class Index extends PageCodeBase {
	
	//MANAGED BEANS
	// News items
	protected NewsItem newsItem;
	private Integer selectedNewItemID;
	private List<Integer> listID;
	private List<Integer> listIDSPag;
	private int paginatedCurrentPage, paginatedNumPages;
	private String pagMsg;
	private List<String> NewsAuthNames;

	// Author items
	private int selectedAuthorID;
	protected Author author;
	private List<String> authorNames;

	Logger log = Logger.getLogger(this.getClass().getSimpleName());
	
	// EJBs
	@EJB
	private UserController uc;
	@EJB
	private NewsController nc;
	@EJB
	private AuthorController ac;
	@EJB
	private AddressController adc;
	
	//Auto-Generated
	protected HtmlScriptCollector scriptCollector1;
	protected HtmlForm form1;
	protected HtmlCommandExButton btnLogout;
	protected HtmlOutputText text15;
	protected HtmlBfPanel tabNews;
	protected HtmlTabbedPanel tabbedPanelMain;
	protected HtmlSelectOneListbox listbox1;
	protected UISelectItems selectItems3;
	protected HtmlCommandExButton btnPagPrevious;
	protected HtmlCommandExButton btnPagNext;
	protected HtmlOutputText textBoxNewsIDSPagCurrent;
	protected HtmlPanelGroup groupBoxNews;
	protected UISelectItems selectItems1;
	protected HtmlCommandExButton btnNewsSaveNew;
	protected HtmlOutputText lblNewsTitle;
	protected HtmlInputText txtBoxNewsTitle;
	protected HtmlOutputText lblNewsDateCreate;
	protected HtmlInputText txtBoxNewsDateCreate;
	protected HtmlOutputText lblNewsDateMod;
	protected HtmlInputText txtBoxNewsDateMod;
	protected HtmlOutputText lblNewsDetails;
	protected HtmlInputTextarea textAreaNewsDetails;
	protected HtmlOutputText lblNewsAuthors;
	protected HtmlSelectManyListbox listBoxAuthors;
	protected HtmlCommandExButton btnNewsSaveEdits;
	protected HtmlCommandExButton btnNewsDelete;
	protected HtmlPanelGroup groupBoxAuthor;
	protected HtmlBfPanel tabAuthors;
	protected HtmlCommandExButton tabbedPanel1_back;
	protected HtmlCommandExButton tabbedPanel1_next;
	protected HtmlCommandExButton tabbedPanel1_finish;
	protected HtmlCommandExButton tabbedPanel1_cancel;
	protected HtmlAjaxRefreshRequest ajaxRefreshRequest2;
	protected HtmlAjaxRefreshRequest ajaxRefreshRequest1;
	protected UISelectItem selectItem2;
	protected HtmlOutputText outputSelectNewsItem;
	protected HtmlCommandExButton button1;
	protected HtmlSelectOneListbox listbox2;
	protected HtmlOutputText lblFname;
	protected HtmlOutputText lblMname;
	protected HtmlOutputText lblLname;
	protected HtmlOutputText lblEmail;
	protected HtmlOutputText lblPhoneNumber;
	protected HtmlOutputText lblDOB;
	protected HtmlOutputText lblCountry;
	protected HtmlOutputText lblCity;
	protected HtmlInputText text1;
	protected HtmlInputText text2;
	protected HtmlInputText text3;
	protected HtmlInputText text4;
	protected HtmlInputText text5;
	protected HtmlInputText text6;
	protected HtmlSelectOneMenu menu1;
	protected HtmlSelectOneMenu menu2;
	public Index(){
		log.info("\n\n\n\n\n**********\nCUSTOM-MSG CONSTRUCTOR: ENTER \n**********\n\n\n\n\n");
		author = new Author();
		newsItem = new NewsItem();
		log.info("\n\n\n\n\n**********\nCUSTOM-MSG CONSTRUCTOR: EXIT \n**********\n\n\n\n\n");
	}
	
	public List<String> getAuthorNames() {
		return authorNames;
	}

	public void setAuthorNames(List<String> authorNames) {
		this.authorNames = authorNames;
	}

	public Integer getSelectedNewItemID() {
		return selectedNewItemID;
	}

	public void setSelectedNewItemID(Integer selectedNewItemID) {
		this.selectedNewItemID = selectedNewItemID;
	}

	public NewsItem getNewsItem() {
		if(newsItem == null)
			newsItem = new NewsItem();
		return newsItem;
	}

	public void setNewsItem(NewsItem newsItem) {
		this.newsItem = newsItem;
	}

	public Author getAuthor() {
		if(author == null)
			author = new Author();
		return author;
	}

	public void setAuthor(Author author) {
		this.author = author;
	}

	protected HtmlScriptCollector getScriptCollector1() {
		if (scriptCollector1 == null) {
			scriptCollector1 = (HtmlScriptCollector) findComponentInRoot("scriptCollector1");
		}
		return scriptCollector1;
	}

	protected HtmlForm getForm1() {
		if (form1 == null) {
			form1 = (HtmlForm) findComponentInRoot("form1");
		}
		return form1;
	}

	protected HtmlCommandExButton getBtnLogout() {
		if (btnLogout == null) {
			btnLogout = (HtmlCommandExButton) findComponentInRoot("btnLogout");
		}
		return btnLogout;
	}

	protected HtmlOutputText getText15() {
		if (text15 == null) {
			text15 = (HtmlOutputText) findComponentInRoot("text15");
		}
		return text15;
	}


	@PostConstruct
	public void onLoad(){
		if(!checkAuth())
			doBtnLogoutAction();
		listID = null;
		setListID(loadNewsIDs());
		loadPaginatedList();
		updatePagMsg();
	}
	
	public void updatePagMsg(){
		pagMsg = "Page " + (paginatedCurrentPage+1) + " of " + paginatedNumPages + ".";
	}
	
	public void loadPaginatedList(){
		listIDSPag = new ArrayList<Integer>();
		for(int i = 0; i < 10; i++){
			if(i + (10 * paginatedCurrentPage) == listID.size())
				break;
			listIDSPag.add(listID.get(i + (10 * paginatedCurrentPage)));
		}
		updatePagMsg();
	}
	
	public String doBtnLogoutAction(){
		CurrentUser.setPassword("");
		CurrentUser.setUsername("");
		return "logout";
	}
	
	public List<Integer> loadNewsIDs(){
		List<Integer> idsList = null;
		if(nc == null)
			nc = new NewsController();
		try {
			idsList = nc.getAllNewsIDs();
			paginatedNumPages = ((idsList.size() + 9) / 10);
			paginatedCurrentPage = 0;
			log.info("\n\n\n\n\n**********\nCUSTOM-MSG LIST SIZE: " + idsList.size() + "\nPAGINATED NUM PAGES: " + paginatedNumPages + "\n**********\n\n\n\n\n");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return idsList;
	}
	
	public boolean checkAuth(){
		User u = new User();
		u.setPassword(CurrentUser.getPassword());
		u.setUsername(CurrentUser.getUsername());
		if(uc == null)
			uc = new UserController();
		return uc.checkAuthentication(u);
	}

	public int getSelectedAuthorID() {
		return selectedAuthorID;
	}

	public void setSelectedAuthorID(int selectedAuthorID) {
		this.selectedAuthorID = selectedAuthorID;
	}

	public List<Integer> getListID() {
		return listID;
	}

	public void setListID(List<Integer> listID) {
		this.listID = listID;
	}

	public List<Integer> getListIDSPag() {
		return listIDSPag;
	}

	public void setListIDSPag(List<Integer> listIDSPag) {
		this.listIDSPag = listIDSPag;
	}

	public void handleCmbBoxNewsIDsValueChange(ValueChangeEvent valueChangedEvent) {
		// Type Java code to handle value changed event here
		log.info("\n\n\n\n\n**********\nCUSTOM-MSG OLD VALUE: " + valueChangedEvent.getOldValue() + "\n**********\n\n\n\n\n");
		log.info("\n\n\n\n\n**********\nCUSTOM-MSG NEW VALUE: " + valueChangedEvent.getNewValue() + "\n**********\n\n\n\n\n");
		selectedNewItemID = Integer.parseInt(valueChangedEvent.getNewValue().toString());
		log.info("\n\n\n\n\nCUSTOM-MSG NEWS ID: " + selectedNewItemID.toString());
		log.info("\n\n\n\n\n**********\nCUSTOM-MSG NEWS ID: " + selectedNewItemID.toString() + "\n**********\n\n\n\n\n");
		clearNewsUIItems();
		// Note: valueChangeEvent contains new and old values
	}
	
	public void clearNewsUIItems(){
		try {
			txtBoxNewsDateCreate.resetValue();
			txtBoxNewsDateMod.resetValue();
			textAreaNewsDetails.resetValue();
			txtBoxNewsTitle.resetValue();
			listBoxAuthors.resetValue();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public String doButton1Action() {
		// This is java code that runs when this action method is invoked
		if(nc == null)
			nc = new NewsController();
		newsItem = null;
		newsItem = nc.getNewsItemByID(selectedNewItemID);
		try {
			NewsAuthNames = null;
			NewsAuthNames = nc.getNewsItemAuthNames(selectedNewItemID);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			NewsAuthNames = new ArrayList<String>();
			for(int i = 0; i < newsItem.getAuthors().size(); i++){
				NewsAuthNames.add(newsItem.getAuthors().get(i).getFname() + " " + newsItem.getAuthors().get(i).getMname() + " " + 
						newsItem.getAuthors().get(i).getLname());
			}
		}
		// TODO: Return an outcome that corresponds to a navigation rule
		// return "home"; // global 
		// return "logout"; // global 
		// return "login-success"; // global 
		// return "login-failure"; // global 
		return "";
	}

	public List<String> getNewsAuthNames() {
		return NewsAuthNames;
	}

	public void setNewsAuthNames(List<String> newsAuthNames) {
		NewsAuthNames = newsAuthNames;
	}
	
	public String doBtnNewsSaveNewAction() {
		// This is java code that runs when this action method is invoked
		if(newsItem != null){
			boolean success = nc.saveNewNewsItem(newsItem);
			if(success){
				log.info("\n\n\n\n\n**********\nCUSTOM-MSG Saved Successfully\n**********\n\n\n\n\n");
			}
		}
		// TODO: Return an outcome that corresponds to a navigation rule
		// return "home"; // global 
		// return "logout"; // global 
		// return "login-success"; // global 
		// return "login-failure"; // global 
		return "";
	}

	public String doBtnNewsSaveEditsAction() {
		// This is java code that runs when this action method is invoked
		boolean success = nc.saveEdits(newsItem);
		if(success){
			log.info("\n\n\n\n\n**********\nCUSTOM-MSG EDITS Saved Successfully\n**********\n\n\n\n\n");
		}
		// TODO: Return an outcome that corresponds to a navigation rule
		// return "home"; // global 
		// return "logout"; // global 
		// return "login-success"; // global 
		// return "login-failure"; // global 
		return "";
	}
	
	public String doBtnNewsDeleteAction() {
		// This is java code that runs when this action method is invoked
		if(nc.deleteItem(newsItem)){
			log.info("\n\n\n\n\n**********\nCUSTOM-MSG deletion successful\n**********\n\n\n\n\n");
			newsItem = new NewsItem();
			clearNewsUIItems();
			loadNewsIDs();
		}
		else
			log.info("\n\n\n\n\n**********\nCUSTOM-MSG deletion failed\n**********\n\n\n\n\n");
		return "";
	}

	protected HtmlBfPanel getTabNews() {
		if (tabNews == null) {
			tabNews = (HtmlBfPanel) findComponentInRoot("tabNews");
		}
		return tabNews;
	}

	protected HtmlTabbedPanel getTabbedPanelMain() {
		if (tabbedPanelMain == null) {
			tabbedPanelMain = (HtmlTabbedPanel) findComponentInRoot("tabbedPanelMain");
		}
		return tabbedPanelMain;
	}

	public String getPagMsg() {
		return pagMsg;
	}

	public void setPagMsg(String pagMsg) {
		this.pagMsg = pagMsg;
	}

	protected HtmlSelectOneListbox getListbox1() {
		if (listbox1 == null) {
			listbox1 = (HtmlSelectOneListbox) findComponentInRoot("listbox1");
		}
		return listbox1;
	}

	protected UISelectItems getSelectItems3() {
		if (selectItems3 == null) {
			selectItems3 = (UISelectItems) findComponentInRoot("selectItems3");
		}
		return selectItems3;
	}

	protected HtmlCommandExButton getBtnPagPrevious() {
		if (btnPagPrevious == null) {
			btnPagPrevious = (HtmlCommandExButton) findComponentInRoot("btnPagPrevious");
		}
		return btnPagPrevious;
	}

	protected HtmlCommandExButton getBtnPagNext() {
		if (btnPagNext == null) {
			btnPagNext = (HtmlCommandExButton) findComponentInRoot("btnPagNext");
		}
		return btnPagNext;
	}

	protected HtmlOutputText getTextBoxNewsIDSPagCurrent() {
		if (textBoxNewsIDSPagCurrent == null) {
			textBoxNewsIDSPagCurrent = (HtmlOutputText) findComponentInRoot("textBoxNewsIDSPagCurrent");
		}
		return textBoxNewsIDSPagCurrent;
	}

	protected HtmlPanelGroup getGroupBoxNews() {
		if (groupBoxNews == null) {
			groupBoxNews = (HtmlPanelGroup) findComponentInRoot("groupBoxNews");
		}
		return groupBoxNews;
	}

	protected UISelectItems getSelectItems1() {
		if (selectItems1 == null) {
			selectItems1 = (UISelectItems) findComponentInRoot("selectItems1");
		}
		return selectItems1;
	}

	protected HtmlCommandExButton getBtnNewsSaveNew() {
		if (btnNewsSaveNew == null) {
			btnNewsSaveNew = (HtmlCommandExButton) findComponentInRoot("btnNewsSaveNew");
		}
		return btnNewsSaveNew;
	}

	protected HtmlOutputText getLblNewsTitle() {
		if (lblNewsTitle == null) {
			lblNewsTitle = (HtmlOutputText) findComponentInRoot("lblNewsTitle");
		}
		return lblNewsTitle;
	}

	protected HtmlInputText getTxtBoxNewsTitle() {
		if (txtBoxNewsTitle == null) {
			txtBoxNewsTitle = (HtmlInputText) findComponentInRoot("txtBoxNewsTitle");
		}
		return txtBoxNewsTitle;
	}

	protected HtmlOutputText getLblNewsDateCreate() {
		if (lblNewsDateCreate == null) {
			lblNewsDateCreate = (HtmlOutputText) findComponentInRoot("lblNewsDateCreate");
		}
		return lblNewsDateCreate;
	}

	protected HtmlInputText getTxtBoxNewsDateCreate() {
		if (txtBoxNewsDateCreate == null) {
			txtBoxNewsDateCreate = (HtmlInputText) findComponentInRoot("txtBoxNewsDateCreate");
		}
		return txtBoxNewsDateCreate;
	}

	protected HtmlOutputText getLblNewsDateMod() {
		if (lblNewsDateMod == null) {
			lblNewsDateMod = (HtmlOutputText) findComponentInRoot("lblNewsDateMod");
		}
		return lblNewsDateMod;
	}

	protected HtmlInputText getTxtBoxNewsDateMod() {
		if (txtBoxNewsDateMod == null) {
			txtBoxNewsDateMod = (HtmlInputText) findComponentInRoot("txtBoxNewsDateMod");
		}
		return txtBoxNewsDateMod;
	}

	protected HtmlOutputText getLblNewsDetails() {
		if (lblNewsDetails == null) {
			lblNewsDetails = (HtmlOutputText) findComponentInRoot("lblNewsDetails");
		}
		return lblNewsDetails;
	}

	protected HtmlInputTextarea getTextAreaNewsDetails() {
		if (textAreaNewsDetails == null) {
			textAreaNewsDetails = (HtmlInputTextarea) findComponentInRoot("textAreaNewsDetails");
		}
		return textAreaNewsDetails;
	}

	protected HtmlOutputText getLblNewsAuthors() {
		if (lblNewsAuthors == null) {
			lblNewsAuthors = (HtmlOutputText) findComponentInRoot("lblNewsAuthors");
		}
		return lblNewsAuthors;
	}

	protected HtmlSelectManyListbox getListBoxAuthors() {
		if (listBoxAuthors == null) {
			listBoxAuthors = (HtmlSelectManyListbox) findComponentInRoot("listBoxAuthors");
		}
		return listBoxAuthors;
	}

	protected HtmlCommandExButton getBtnNewsSaveEdits() {
		if (btnNewsSaveEdits == null) {
			btnNewsSaveEdits = (HtmlCommandExButton) findComponentInRoot("btnNewsSaveEdits");
		}
		return btnNewsSaveEdits;
	}

	protected HtmlCommandExButton getBtnNewsDelete() {
		if (btnNewsDelete == null) {
			btnNewsDelete = (HtmlCommandExButton) findComponentInRoot("btnNewsDelete");
		}
		return btnNewsDelete;
	}

	protected HtmlPanelGroup getGroupBoxAuthor() {
		if (groupBoxAuthor == null) {
			groupBoxAuthor = (HtmlPanelGroup) findComponentInRoot("groupBoxAuthor");
		}
		return groupBoxAuthor;
	}

	protected HtmlBfPanel getTabAuthors() {
		if (tabAuthors == null) {
			tabAuthors = (HtmlBfPanel) findComponentInRoot("tabAuthors");
		}
		return tabAuthors;
	}

	protected HtmlCommandExButton getTabbedPanel1_back() {
		if (tabbedPanel1_back == null) {
			tabbedPanel1_back = (HtmlCommandExButton) findComponentInRoot("tabbedPanel1_back");
		}
		return tabbedPanel1_back;
	}

	protected HtmlCommandExButton getTabbedPanel1_next() {
		if (tabbedPanel1_next == null) {
			tabbedPanel1_next = (HtmlCommandExButton) findComponentInRoot("tabbedPanel1_next");
		}
		return tabbedPanel1_next;
	}

	protected HtmlCommandExButton getTabbedPanel1_finish() {
		if (tabbedPanel1_finish == null) {
			tabbedPanel1_finish = (HtmlCommandExButton) findComponentInRoot("tabbedPanel1_finish");
		}
		return tabbedPanel1_finish;
	}

	protected HtmlCommandExButton getTabbedPanel1_cancel() {
		if (tabbedPanel1_cancel == null) {
			tabbedPanel1_cancel = (HtmlCommandExButton) findComponentInRoot("tabbedPanel1_cancel");
		}
		return tabbedPanel1_cancel;
	}

	protected HtmlAjaxRefreshRequest getAjaxRefreshRequest2() {
		if (ajaxRefreshRequest2 == null) {
			ajaxRefreshRequest2 = (HtmlAjaxRefreshRequest) findComponentInRoot("ajaxRefreshRequest2");
		}
		return ajaxRefreshRequest2;
	}

	protected HtmlAjaxRefreshRequest getAjaxRefreshRequest1() {
		if (ajaxRefreshRequest1 == null) {
			ajaxRefreshRequest1 = (HtmlAjaxRefreshRequest) findComponentInRoot("ajaxRefreshRequest1");
		}
		return ajaxRefreshRequest1;
	}

	protected UISelectItem getSelectItem2() {
		if (selectItem2 == null) {
			selectItem2 = (UISelectItem) findComponentInRoot("selectItem2");
		}
		return selectItem2;
	}

	public String doBtnPagPreviousAction() {
		// This is java code that runs when this action method is invoked
		if(paginatedCurrentPage == 0)
			return "";
		else {
			paginatedCurrentPage--;
			loadPaginatedList();
		}
		// TODO: Return an outcome that corresponds to a navigation rule
		// return "home"; // global 
		// return "logout"; // global 
		// return "login-success"; // global 
		// return "login-failure"; // global 
		return "";
	}

	public String doBtnPagNextAction() {
		// This is java code that runs when this action method is invoked
		if(paginatedCurrentPage >= paginatedNumPages - 1)
			return "";
		else {
			paginatedCurrentPage++;
			loadPaginatedList();
		}
		// TODO: Return an outcome that corresponds to a navigation rule
		// return "home"; // global 
		// return "logout"; // global 
		// return "login-success"; // global 
		// return "login-failure"; // global 
		return "";
	}

	public void handleListbox1ValueChange(ValueChangeEvent valueChangedEvent) {
		// Type Java code to handle value changed event here
		log.info("\n\n\n\n\n**********\nCUSTOM-MSG OLD VALUE: " + valueChangedEvent.getOldValue() + "\n**********\n\n\n\n\n");
		log.info("\n\n\n\n\n**********\nCUSTOM-MSG NEW VALUE: " + valueChangedEvent.getNewValue() + "\n**********\n\n\n\n\n");
		selectedNewItemID = Integer.parseInt(valueChangedEvent.getNewValue().toString());
		log.info("\n\n\n\n\nCUSTOM-MSG NEWS ID: " + selectedNewItemID.toString());
		log.info("\n\n\n\n\n**********\nCUSTOM-MSG NEWS ID: " + selectedNewItemID.toString() + "\n**********\n\n\n\n\n");
		clearNewsUIItems();
		// Note: valueChangeEvent contains new and old values
	}

	protected HtmlOutputText getOutputSelectNewsItem() {
		if (outputSelectNewsItem == null) {
			outputSelectNewsItem = (HtmlOutputText) findComponentInRoot("outputSelectNewsItem");
		}
		return outputSelectNewsItem;
	}

	protected HtmlCommandExButton getButton1() {
		if (button1 == null) {
			button1 = (HtmlCommandExButton) findComponentInRoot("button1");
		}
		return button1;
	}

	protected HtmlSelectOneListbox getListbox2() {
		if (listbox2 == null) {
			listbox2 = (HtmlSelectOneListbox) findComponentInRoot("listbox2");
		}
		return listbox2;
	}

	protected HtmlOutputText getLblFname() {
		if (lblFname == null) {
			lblFname = (HtmlOutputText) findComponentInRoot("lblFname");
		}
		return lblFname;
	}

	protected HtmlOutputText getLblMname() {
		if (lblMname == null) {
			lblMname = (HtmlOutputText) findComponentInRoot("lblMname");
		}
		return lblMname;
	}

	protected HtmlOutputText getLblLname() {
		if (lblLname == null) {
			lblLname = (HtmlOutputText) findComponentInRoot("lblLname");
		}
		return lblLname;
	}

	protected HtmlOutputText getLblEmail() {
		if (lblEmail == null) {
			lblEmail = (HtmlOutputText) findComponentInRoot("lblEmail");
		}
		return lblEmail;
	}

	protected HtmlOutputText getLblPhoneNumber() {
		if (lblPhoneNumber == null) {
			lblPhoneNumber = (HtmlOutputText) findComponentInRoot("lblPhoneNumber");
		}
		return lblPhoneNumber;
	}

	protected HtmlOutputText getLblDOB() {
		if (lblDOB == null) {
			lblDOB = (HtmlOutputText) findComponentInRoot("lblDOB");
		}
		return lblDOB;
	}

	protected HtmlOutputText getLblCountry() {
		if (lblCountry == null) {
			lblCountry = (HtmlOutputText) findComponentInRoot("lblCountry");
		}
		return lblCountry;
	}

	protected HtmlOutputText getLblCity() {
		if (lblCity == null) {
			lblCity = (HtmlOutputText) findComponentInRoot("lblCity");
		}
		return lblCity;
	}

	protected HtmlInputText getText1() {
		if (text1 == null) {
			text1 = (HtmlInputText) findComponentInRoot("text1");
		}
		return text1;
	}

	protected HtmlInputText getText2() {
		if (text2 == null) {
			text2 = (HtmlInputText) findComponentInRoot("text2");
		}
		return text2;
	}

	protected HtmlInputText getText3() {
		if (text3 == null) {
			text3 = (HtmlInputText) findComponentInRoot("text3");
		}
		return text3;
	}

	protected HtmlInputText getText4() {
		if (text4 == null) {
			text4 = (HtmlInputText) findComponentInRoot("text4");
		}
		return text4;
	}

	protected HtmlInputText getText5() {
		if (text5 == null) {
			text5 = (HtmlInputText) findComponentInRoot("text5");
		}
		return text5;
	}

	protected HtmlInputText getText6() {
		if (text6 == null) {
			text6 = (HtmlInputText) findComponentInRoot("text6");
		}
		return text6;
	}

	protected HtmlSelectOneMenu getMenu1() {
		if (menu1 == null) {
			menu1 = (HtmlSelectOneMenu) findComponentInRoot("menu1");
		}
		return menu1;
	}

	protected HtmlSelectOneMenu getMenu2() {
		if (menu2 == null) {
			menu2 = (HtmlSelectOneMenu) findComponentInRoot("menu2");
		}
		return menu2;
	}
}